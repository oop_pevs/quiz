﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace zadanie2
{
    public class OneQuestion
    {
        public string text { get; set; }
        public string correct { get; set; }
        public List<string> answers { get; set; }
        public string image { get; set; }
    }

    public class Questions
    {
        public List<OneQuestion> questions { get; set; }
    }

    public partial class MainPage : ContentPage
    {
        int position = 0;
        int points = 0;
        static string text = File.ReadAllText(@"/Users/eriklaco/Projects/zadanie2/zadanie2/questions.json");
        Questions obj = JsonConvert.DeserializeObject<Questions>(text);

        public MainPage()
        {
            InitializeComponent();
            loadQuestion();

        }

        public void loadQuestion()
        {
            btnAnswer1.IsVisible = true;
            btnAnswer2.IsVisible = true;
            btnAnswer3.IsVisible = true;
            btnAnswer4.IsVisible = true;

            if (position >= obj.questions.Count) {
                clearButtons();
                lblQuestion.Text = String.Format("Score: {0:F0} points", points);
                btnRestart.Text = "Restart game";
            } else {
                lblQuestion.Text = obj.questions[position].text;
                btnAnswer1.Text = obj.questions[position].answers[0];
                btnAnswer2.Text = obj.questions[position].answers[1];
                btnAnswer3.Text = obj.questions[position].answers[2];
                btnAnswer4.Text = obj.questions[position].answers[3];

                if (!String.IsNullOrEmpty(obj.questions[position].image)) {
                    image.Source = new UriImageSource { Uri = new Uri(obj.questions[position].image) };
                } else {
                    image.Source = "";
                }
            }
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            string s = (sender as Button).Text;
            if (s == obj.questions[position].correct)
            {
                this.points++;
            }

            this.position++;
            loadQuestion();
        }

        private void ReorderAnswers()
        {
            foreach (OneQuestion oneQuestion in this.obj.questions) {
                List<string> answers = oneQuestion.answers;
                foreach (string oneAnswer in answers) {

                }
            }
        }

        void RestartGame(object sender, System.EventArgs e)
        {
            this.position = 0;
            this.points = 0;
            loadQuestion();
            btnRestart.Text = "";
        }

        public void clearButtons()
        {
            btnAnswer1.IsVisible = false;
            btnAnswer2.IsVisible = false;
            btnAnswer3.IsVisible = false;
            btnAnswer4.IsVisible = false;
            image.Source = "";

        }
    }
}
